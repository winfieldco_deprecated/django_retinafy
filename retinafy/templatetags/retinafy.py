from django import template
from django.conf import settings
from django.template import VariableDoesNotExist
register = template.Library()
import logging

class Retinafy(template.Node):
    def __init__(self, image_url, width, height):
        self.width = width
        self.height = height
        self.image_url = image_url

    def render(self, context):

        # Check if image url is a variable and if so use it
        try:
          image_url_var = template.Variable(self.image_url)
          image_url_var_resolved = image_url_var.resolve(context)
          if image_url_var_resolved:
            self.image_url = image_url_var_resolved
        except VariableDoesNotExist:
          pass

        # If not an external link, assume its a static file
        if not 'http' in self.image_url:
            self.image_url = settings.STATIC_URL + self.image_url

        # Append the @2x
        name = self.image_url[:self.image_url.rfind(".")]
        ext = self.image_url[self.image_url.rfind("."):]

        self.image_url = name + '@2x' + ext

        return 'width="' + self.width.strip() + '"' + ' height="' + self.height.strip() + '"' + ' src="' + self.image_url + '"';

def do_retinafy(parser, token):
    try:
        # split_contents() knows not to split quoted strings.
        tag_name, image_url, width, height = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r tag requires two arguments, the content" % token.contents.split()[0])

    return Retinafy(image_url, width, height)

do_retinafy = register.tag('retinafy', do_retinafy)